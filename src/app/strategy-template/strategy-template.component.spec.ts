import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StrategyTemplateComponent } from './strategy-template.component';

describe('StrategyTemplateComponent', () => {
  let component: StrategyTemplateComponent;
  let fixture: ComponentFixture<StrategyTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StrategyTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StrategyTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
