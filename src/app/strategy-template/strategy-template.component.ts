import { Component, OnInit } from '@angular/core';
import { Ticker, RestService } from '../common/rest.service';
import { Strategy, StrategyService } from '../common/strategy.service';

@Component({
  selector: 'app-strategy-template',
  templateUrl: './strategy-template.component.html',
  styleUrls: ['./strategy-template.component.css']
})

export class StrategyTemplateComponent implements OnInit {

  // Local Form Variable Storage (2 Way Binded)
  strategyCreated: Strategy;
  strategyName: string;
  strategyTicker: Ticker;
  strategyStrategy: string;
  strategySize: number;
  strategysdHi: number;
  strategysdLo: number;
  strategyplStop: number;

  // Form Outputs
  tickers: Ticker[] = [];
  usedTickers: Ticker[] = [];
  strategies: Array<string> = [];

  tickerDisabled = true;
  nameDisabled = false;

  constructor(private rs: RestService, private ss: StrategyService) {
    rs.getTickers().subscribe(response =>  {
      this.tickers = response;
    });
    this.strategies = ss.getStrategies();
  }

  ngOnInit() {
  }

  createStrategy() {
    this.ss.createStrategy(this.strategyplStop, this.strategyName,
      this.strategySize, this.strategyTicker, 1, this.strategysdHi, this.strategysdLo, 0);
  }

  deleteStrategy() {
    if (this.strategyCreated == null) {
      this.strategyName = null;
      this.strategyTicker = null;
      this.strategyStrategy = null;
      this.strategySize = null;
      this.strategysdLo = null;
      this.strategysdHi = null;
      this.strategyplStop = null;
    } else {
      this.ss.deleteStrategy(this.strategyCreated);
      this.strategyCreated = null;
    }
  }

  checkUsedTicker() {
    if (this.ss.getCreatedStrategies().length === 0) {
      this.tickerDisabled = false;
      return;
    }
    for (let currStrat of this.ss.getCreatedStrategies()) {
      if (currStrat.stock.ticker === this.strategyTicker.ticker) {
        this.tickerDisabled = true;
        break;
      } else { this.tickerDisabled = false;
      }
    }
  }

  checkUsedName() {
    if (this.ss.getCreatedStrategies().length === 0) {
      this.nameDisabled = false;
      return;
    }
    for (let currStrat of this.ss.getCreatedStrategies()) {
      if (currStrat.name === this.strategyName) {
        this.nameDisabled = true;
        break;
      } else { this.nameDisabled = false;
      }
    }
  }

  clearForms() {
    if (this.strategyCreated == null) {
      this.strategyName = null;
      this.strategyTicker = null;
      this.strategyStrategy = null;
      this.strategySize = null;
      this.strategysdLo = null;
      this.strategysdHi = null;
      this.strategyplStop = null;
    } else {
      this.strategyCreated = null;
    }
  }
}
