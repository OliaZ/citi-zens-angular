import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';

import { AppComponent } from './app.component';
import { StrategyTemplateComponent } from './strategy-template/strategy-template.component';
import { RestService } from './common/rest.service';
import { RunStrategyComponent } from './run-strategy/run-strategy.component';
import {StrategyService} from './common/strategy.service';
import { AnalyzeTradesComponent } from './analyze-trades/analyze-trades.component';
import { NavbarComponent } from './navbar/navbar.component';
import {MatFormFieldModule} from '@angular/material';
import {MatSelectModule} from '@angular/material';
import {MatInputModule} from '@angular/material';
import { AnalyzeChartsComponent } from './analyze-charts/analyze-charts.component';
import {ChartsModule} from 'ng2-charts';

@NgModule({
  declarations: [
    AppComponent,
    StrategyTemplateComponent,
    RunStrategyComponent,
    AnalyzeTradesComponent,
    NavbarComponent,
    AnalyzeChartsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    NgbModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatCheckboxModule,
    MatButtonModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    ChartsModule
  ],
  providers: [RestService, StrategyService],
  bootstrap: [AppComponent]
})
export class AppModule { }
