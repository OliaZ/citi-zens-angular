import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Strategy } from './strategy.service';

export class Analytics {
  constructor(
    public avg: Array<number>,
    public high: Array<number>,
    public labels: Array<number>,
    public low: Array<number>,
    public price: Array<number>) {}
}

export class Ticker {
  constructor(
    public id: number,
    public ticker: string) {
  }
}

export class Trade {
  constructor(
    public id: number,
    public price: number,
    public size: number,
    public stock: Ticker,
    public tempStockTicker: string,
    public strategy: string,
    public lastStateChange: string,
    public tradeType: string,
    public state: string,
    public stockTicker: string) { }
}

@Injectable({
  providedIn: 'root'
})
export class RestService {

  constructor(private httpClient: HttpClient) { }

  getTickers(): Observable<Ticker[]> {
    return this.httpClient.get<Ticker[]>('/stock');
  }

  getTrades(): Observable<Trade[]> {
    return this.httpClient.get<Trade[]>('/trade');
  }

  getStrategies(): Observable<Strategy[]> {
    return this.httpClient.get<Strategy[]>('/strategy');
  }

  getAnalytics(strategyName: string, numPoints: number): Observable<Analytics> {
    return this.httpClient.get<Analytics>('/analytics' + strategyName + '/' + numPoints);
  }

  pushStrategy(strategy: Strategy): Observable<Strategy> {
    return this.httpClient.post<Strategy>('/strategy', strategy);
  }

  startStrategy(strategyName: string): Observable<string> {
    return this.httpClient.post<string>('/strategy/start', strategyName);
  }

  stopStrategy(strategyName: string): Observable<string> {
    return this.httpClient.post<string>('/strategy/stop', strategyName);
  }

  deleteStrategy(strategyName: string): Observable<string> {
    return this.httpClient.delete<string>('/strategy/' + strategyName);
  }

}
