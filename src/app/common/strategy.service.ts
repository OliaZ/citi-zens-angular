import {Injectable, OnInit} from '@angular/core';
import {RestService, Ticker} from './rest.service';

export class Strategy {
  constructor(
    public exitThreshold: number,
    public name: string,
    public size: number,
    public stock: Ticker,
    public stopped: number,
    public volMultipleHigh: number,
    public volMultipleLow: number,
    public profit: number) {
  }
}

@Injectable({
  providedIn: 'root'
})

export class StrategyService {

  createdStrategies: Array<Strategy> = [];
  responseStrategy: Array<Strategy> = [];
  startStopResponse: string;
  deleteResponse: string;

  constructor(private rs: RestService) { }

  createStrategy(eT: number, n: string, s: number, t: Ticker, stop: number, vH: number, vL: number, p: number) {
    this.createdStrategies.push(new Strategy(eT, n, s, t, stop, vH, vL, p));
    this.rs
      .pushStrategy(this.createdStrategies[this.createdStrategies.length - 1])
      .subscribe(response => this.responseStrategy.push(response));
  }

  createStrategyNonDB(eT: number, n: string, s: number, t: Ticker, stop: number, vH: number, vL: number, p: number) {
    this.createdStrategies.push(new Strategy(eT, n, s, t, stop, vH, vL, p));
  }

  deleteStrategy(s: Strategy) {
    const index = this.createdStrategies.indexOf(s, 0);
    if (index > -1) {
      this.rs
        .deleteStrategy(this.createdStrategies[index].name)
        .subscribe(response => this.deleteResponse = response);
      this.createdStrategies.splice(index, 1);
    }
  }

  getCreatedStrategies() {
    return this.createdStrategies;
  }

  getStrategies() {
    return ['Bollinger Bands'];
  }

  startStrategy(index: number) {
    this.createdStrategies[index].stopped = 0;
    console.log('Starting Strategy ' + this.createdStrategies[index].name);
    this.rs
      .startStrategy(this.createdStrategies[this.createdStrategies.length - 1].name)
      .subscribe(response => this.startStopResponse = response);
    }

  stopStrategy(index: number) {
    this.createdStrategies[index].stopped = 1;
    console.log('Stopping Strategy ' + this.createdStrategies[index].name);
    this.rs
      .stopStrategy(this.createdStrategies[this.createdStrategies.length - 1].name)
      .subscribe(response => this.startStopResponse = response);
    }

}
