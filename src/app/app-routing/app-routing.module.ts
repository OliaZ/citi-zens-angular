import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StrategyTemplateComponent } from '../strategy-template/strategy-template.component';
import { RunStrategyComponent } from '../run-strategy/run-strategy.component';
import { AnalyzeTradesComponent } from '../analyze-trades/analyze-trades.component';

const routes: Routes = [
  {path: '', component: StrategyTemplateComponent},
  {path: 'create', component: StrategyTemplateComponent},
  {path: 'run', component: RunStrategyComponent },
  {path: 'analyze', component: AnalyzeTradesComponent },
  {path: '**', redirectTo: '/home' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }
