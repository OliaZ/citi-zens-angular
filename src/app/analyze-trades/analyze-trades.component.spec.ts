import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnalyzeTradesComponent } from './analyze-trades.component';

describe('AnalyzeTradesComponent', () => {
  let component: AnalyzeTradesComponent;
  let fixture: ComponentFixture<AnalyzeTradesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnalyzeTradesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnalyzeTradesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
