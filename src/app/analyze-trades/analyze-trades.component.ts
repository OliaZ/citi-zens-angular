import { Component, OnInit } from '@angular/core';
import { Strategy, StrategyService } from '../common/strategy.service';
import {RestService, Trade} from '../common/rest.service';
import { timer } from 'rxjs';


@Component({
  selector: 'app-analyze-trades',
  templateUrl: './analyze-trades.component.html',
  styleUrls: ['./analyze-trades.component.css']
})
export class AnalyzeTradesComponent implements OnInit {

  strategySelected: Strategy;
  trades: Trade[] = [];
  databaseStrategies: Array<Strategy> = [];
  viewOptions: Array<string> = ['Table', 'Graph'];
  viewSelected: string;

  constructor(private ss: StrategyService, private rs: RestService) {
  }

  ngOnInit() {
    this.allSubscribe();
  }

  allSubscribe() {
    this.rs.getTrades().subscribe(response =>  {
      this.trades = response;
      this.trades = this.trades.reverse();
    });
    this.rs.getStrategies().subscribe(response =>  {
      this.databaseStrategies = response;
      this.updateSelectedStrategy();
    });
    this.updateTimer();
  }

  updateTimer() {
    timer(15000).subscribe( () => this.allSubscribe());
  }

  updateSelectedStrategy() {
    if (!(this.strategySelected == null) && !(this.databaseStrategies == null)) {
      for (let strategy of this.databaseStrategies) {
        if (strategy.name === this.strategySelected.name) {
          this.strategySelected.profit = strategy.profit;
        }
      }
    }
  }

}
