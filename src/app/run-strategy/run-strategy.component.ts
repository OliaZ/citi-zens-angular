import { Component, OnInit } from '@angular/core';
import { Strategy, StrategyService } from '../common/strategy.service';

@Component({
  selector: 'app-run-strategy',
  templateUrl: './run-strategy.component.html',
  styleUrls: ['./run-strategy.component.css']
})
export class RunStrategyComponent implements OnInit {

  strategySelected: Strategy;
  strategies: Array<string> = [];

  constructor(private ss: StrategyService) {
    this.strategies = ss.getStrategies();
  }

  ngOnInit() {
  }

  startStrategy() {
    if (this.strategySelected != null) {
      const index: number = this.ss.getCreatedStrategies().findIndex(i => i.name === this.strategySelected.name);
      this.ss.startStrategy(index);
    }
  }

  stopStrategy() {
    if (this.strategySelected != null) {
      const index: number = this.ss.getCreatedStrategies().findIndex(i => i.name === this.strategySelected.name);
      this.ss.stopStrategy(index);
    }
  }

}
