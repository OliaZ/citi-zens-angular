import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Color, BaseChartDirective, Label } from 'ng2-charts';
import * as pluginAnnotations from 'chartjs-plugin-annotation';
import {Analytics, RestService} from '../common/rest.service';
import {timer} from 'rxjs';

@Component({
  selector: 'app-analyze-charts',
  templateUrl: './analyze-charts.component.html',
  styleUrls: ['./analyze-charts.component.css']
})
export class AnalyzeChartsComponent implements OnInit {
  currentPoints: Analytics = new Analytics([0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]);

  public lineChartData: ChartDataSets[] = [
    { data: this.currentPoints.avg, label: 'Average', fill: false },
    { data: this.currentPoints.high, label: 'SD High', fill: false },
    { data: this.currentPoints.low, label: 'SD Low', fill: false},
    { data: this.currentPoints.price, label: 'Price', fill: false}
  ];
  public lineChartLabels: Label[] = ['-9', '-8', '-7', '-6', '-5', '-4', '-3', '-2', '-1', '0'];
  public lineChartOptions: (ChartOptions & { annotation: any }) = {
    responsive: true,
    scales: {
      // We use this empty structure as a placeholder for dynamic theming.
      xAxes: [{}],
      yAxes: [
        {
          id: 'y-axis-0',
          position: 'left',
        },
      ]
    },
    annotation: {
    },
  };
  public lineChartColors: Color[] = [
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,0.2)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // dark grey
      backgroundColor: 'rgba(34,167,53,0.6)',
      borderColor: 'rgba(34,167,53,0.6)',
      pointBackgroundColor: 'rgba(77,83,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    },
    { // red
      backgroundColor: 'rgba(223,7,39,0.5)',
      borderColor: 'red',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // dark grey
      backgroundColor: 'rgba(0,0,0,1.0)',
      borderColor: 'rgba(0,0,0,1.0)',
      pointBackgroundColor: 'rgba(77,83,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    }
  ];
  public lineChartLegend = true;
  public lineChartType = 'line';
  public lineChartPlugins = [pluginAnnotations];

  @ViewChild(BaseChartDirective, { static: true }) chart: BaseChartDirective;
  @Input() strategySelected;

  constructor(private rs: RestService) {}

  ngOnInit() {
    this.allSubscribe();
  }

  public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  allSubscribe() {
    this.rs.getAnalytics(this.strategySelected.name, 10).subscribe(response =>  {
      this.currentPoints = response;
      this.lineChartData[0].data = response.avg.reverse();
      this.lineChartData[1].data = response.high.reverse();
      this.lineChartData[2].data = response.low.reverse();
      this.lineChartData[3].data = response.price.reverse();
    });
    this.updateTimer();
  }

  updateTimer() {
    timer(15000).subscribe( () => this.allSubscribe());
  }
}
