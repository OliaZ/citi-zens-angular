import { Component } from '@angular/core';
import {RestService} from './common/rest.service';
import {Strategy, StrategyService} from './common/strategy.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Citi-Zens Trade Platform';

  databaseStrategies: Array<Strategy> = [];

  constructor(private rs: RestService, private ss: StrategyService) {
    rs.getStrategies().subscribe(response =>  {
      this.databaseStrategies = response;
      this.updateStrategyService();
    });
  }

  updateStrategyService() {
    for ( let databaseStrategy of this.databaseStrategies) {
      this.ss.createStrategyNonDB(databaseStrategy.exitThreshold, databaseStrategy.name, databaseStrategy.size,
        databaseStrategy.stock, databaseStrategy.stopped, databaseStrategy.volMultipleHigh,
        databaseStrategy.volMultipleLow, databaseStrategy.profit);
    }
  }

}
